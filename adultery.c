#define _GNU_SOURCE
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/sendfile.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <err.h>
#include <dirent.h>
#include <assert.h>
#include <stdbool.h>
#include <wordexp.h>
#include <stdio.h>
#include <argp.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include "lib/cat/cat.h"

// You need to be able to get into the folder as a user
// even if you make it as root, right?
// NOTE: this is probably bad. I use this for everything, dirs, files and
// scripts.
#define PERMISSIONS 0775

#define PROGNAME "adultery"
#define ROOT_DATA_DIR "/usr/local/share/" PROGNAME
#define DEFAULT_USER_DATA_DIR "~/.local/share/" PROGNAME

enum versions {
	VERSION_CLEAN,
	VERSION_ADULTERATED,
};

char *version_strs[] = {
	[VERSION_CLEAN] = "clean",
	[VERSION_ADULTERATED] = "adulterated",
};

enum actions {
	ACTION_NONE,

	ACTION_ON,
	ACTION_OFF,

	ACTION_ADULTERATED,
	ACTION_CLEAN,
	ACTION_LIST,
	ACTION_STATE,
};

enum modifiers {
	MODIFIER_PRINT = 1 << 0,
	MODIFIER_CAT = 1 << 1,
	MODIFIER_IS_SCRIPT = 1 << 2,
};

#if 0
enum LONGOPT {
	LONGOPT_NOTHING,
};
#endif

struct args {
	enum actions action;
	enum modifiers modifiers;
	char *file_or_script;
	int n_positional_args;
};

#define NSCRATCH PATH_MAX
char scratch[PATH_MAX];

// Just asserts, cause I'm the one passing the string.
static void tilde_expand (char *buf, int nbuf, char *path) {
	wordexp_t p;
	int ret;
	if ((ret = wordexp (path, &p, WRDE_NOCMD))) {
		if (ret == WRDE_NOSPACE)
			assert (!"Out of memory");
		else
			assert (!"Passed a bad path");
	}
	char *stpncpy_ret = stpncpy (buf, *p.we_wordv, nbuf);
	if (stpncpy_ret - buf >= nbuf)
		errx (1, "Too big path");
}

// Returns zero if it you were root and one if not. Could use
// an enum or could just not bother at all.
// int nbuf is fine -- it's a directory.
static int get_data_dir (char *buf, int nbuf,
		char *default_root_data_dir, char *default_user_data_dir) {
	int res = 0;
	char *tmp;
	int len;
	if (geteuid () == 0)
		strncpy (buf, default_root_data_dir, nbuf);
	else {
		res = 1;
		if ((tmp = getenv ("xdg-data-dir"))) {
			len = strlen (tmp);
			if (len >= nbuf) return 1;
			strncpy (buf, tmp, len);
		} else
			tilde_expand (buf, nbuf, default_user_data_dir);
	}
	return res;
}


// Returns buf for convenience.
// Is a dumb function, flimsy.
static char *replace_char (char *buf, size_t nbuf, char replace,
		char with) {
	if (*buf == replace) {
		memmove (buf, buf + 1, nbuf);
		nbuf--;
	}
	char *p;
	for (p = buf; p - buf < nbuf; p++) {
		if (*p == replace)
			*p = with;
	}
	buf[nbuf] = '\0';
	return buf;
}

// transform_func is a textual transformation on the string buffer.
// Silly to use a function pointer but fun, right?
static void print_files_in_dir (char *dir, mode_t mode,
		char *(*transform_func) (char *, size_t, char, char)) {
	DIR *dp = opendir (dir);
	if (!dp) err (1, "Couldn't open %s", dir);
	struct dirent *dirent;
	struct stat statbuf;
	char path_name[PATH_MAX];
	while (dirent = readdir (dp)) {
		// I haven't checked if dir is absolute. I don't know if
		// it matters.
		sprintf (path_name, "%s/%s", dir, dirent->d_name);

		if (stat (path_name, &statbuf))
			err (1, "This is probably a programming error");

		// Reusing path_name.
		if (*dirent->d_name != '.'
				&& (statbuf.st_mode & S_IFMT) == mode) {
			if (transform_func) {
				strcpy (path_name, dirent->d_name);
				transform_func (path_name,
						strlen (dirent->d_name), '-', '/');
				printf ("%s\n", path_name);
			} else {
				printf ("%s\n", dirent->d_name);
			}
		}
	}

	closedir (dp);
}

// Returns an int for -1 for error.
int bool_from_string (char *s) {
	if (!strcasecmp (s, "on") || !strcmp (s, "1")
			|| !strcasecmp (s, "true") || !strcasecmp (s, "t")) {
		return 1;
	} else if (!strcasecmp (s, "off") || !strcmp (s, "0")
			|| !strcasecmp (s, "false") || !strcasecmp (s, "f")) {
		return 0;
	}
	return -1;
}

static error_t parse_opt (int key, char *arg, struct argp_state *state) {
	struct args *args = state->input;

#define set_action(_action) \
	do { \
		if (args->action) \
			errx (1, "Only one action allowed"); \
		else args->action = _action; \
	} while (0)

	switch (key) {
		case 'a': set_action (ACTION_ADULTERATED); break;
		case 'c': set_action (ACTION_CLEAN); break;
		case 'C':
			if (args->modifiers & MODIFIER_PRINT)
				argp_failure (state, 1, 0, "--path cancels out --cat");
			args->modifiers |= MODIFIER_CAT;
			break;
		case 'l': set_action (ACTION_LIST); break;
		case 'p':
			if (args->modifiers & MODIFIER_PRINT)
				argp_failure (state, 1, 0, "--cat cancels out --path");
			args->modifiers |= MODIFIER_PRINT;
			break;
		case 's': set_action (ACTION_STATE); break;
		case 'S': args->modifiers |= MODIFIER_IS_SCRIPT; break;

		case ARGP_KEY_ARG:
			// First arg is the file (or script, with --script), second is the
			// action. Any more is err.
			if (args->n_positional_args == 0) {
				args->file_or_script = realpath (arg, NULL);
			} else if (args->n_positional_args == 1) {
				int ret = bool_from_string (arg);
				if (ret == 1) set_action (ACTION_ON);
				else if (ret == 0) set_action (ACTION_OFF);
				else
					argp_failure (state, 1, 0, "\
ACTION must be \"on\" or \"off\" (or equivalent statement, see end)");
			} else {
				argp_failure (state, 1, 0, "Too many positional arguments");
			}
			args->n_positional_args++;
			break;

		case ARGP_KEY_END:
			if (!args->file_or_script)
				argp_failure (state, 1, 0, "Need file");
			break;
	 	default:
			break;
	}
	return 0;
}


static void *xmalloc (size_t bytes) {
	void *res = malloc (bytes);
	if (!res) err (1, "malloc failed");
	return res;
}

static struct args get_args (int argc, char **argv) {
	struct args res = {0};
	struct argp_option options[] = {
		{0, 0, 0, 0, "Actions:", 1},
		{"adulterated", 'a', 0, 0,
			"Open the FILE's ADULTERATED version in EDITOR"},
		{"clean", 'c', 0, 0, "Open the FILE's CLEAN version in EDITOR"},
		{"list", 'l', 0, 0, "List FILES"},
		{"state", 's', 0, 0,
			"Print the state of the file, ie which file is in use"},
		{0, 0, 0, 0, "That modify other options:", 2},
		{"cat", 'C', 0, 0,
			"With -a or -c, cat them instead of opening them"},
		{"script", 'S', 0, 0,
			"When initialising an adulteration, specify you're passing \
a script rather than a file. See end about scripts"},
		{"print", 'p', 0, 0,
			"With -a or -c, print their paths instead of opening them"},
		{0},
	};
	struct argp argp = {
		options, parse_opt, "FILE [action]", "\
Swap between CLEAN and ADULTERATED versions of a file"
"\v"
"Run the program on a FILE, like, \"adultery /etc/hosts\". This will make \
a directory in (if you're root) ~/.local/share/adultery or (if you're not) \
/usr/local/share/adultery called, in this case, etc-hosts. Inside that \
directory you will find two files, \"clean\" and \"adulterated\". clean is \
a copy of the FILE as is right now. adulterated is the file _you_ edit. \
As a convenience, you can do this with \"adultery FILE --adulterated\", but \
make sure EDITOR is set, particularly if you're calling the program as root \
(root doesn't normally have EDITOR set).\n\
\n\
After that, you can do \"adultery FILE on\" and \"adultery FILE off\". \
on overwrites FILE with adulterated and off overwrites it with clean. \
The point of all of this is to let you switch between two config files. \
\n\
If you're running as root, files will go in /usr/local/share/adultery, else \
they will go in ~/.local/share/adultery.\n\
\n\
\"ACTION values. You can do \"on\", \"1\", \"true\" and \"T\" or "
"\"off\", \"0\", \"false\" or \"F\".\n\
\n\
Scripts are a little different to FILEs. When initiating, you pass the script \
and --is-script. The program will copy that script to the \"clean\" file. \
Make your changes to \"adulterated\", as usual. Pass --script again when  \
you do \"adulterated <script> on\". I could record that it's a script in  \
some way so you don't have to, but I won't. Scripts are different, so ...  \
Obviously I am being lazy.\n\
\n\
--state doesn't work with scripts. I can't know whether the script is on or  \
not. You say, well, just save the info in a file. That'd get out of sync  \
immediately. You'd make a change manually.\n\
\n\
When you pass the script, use the scripts name. You can throw the original \
file away. I think this feature will be annoying to sync in your dotfiles."
	};

	argp_parse (&argp, argc, argv, 0, 0, &res);
	return res;
}

int systemf (char *fmt, ...) {
	va_list ap;
	va_start (ap, fmt);
	char *str = NULL;
	vasprintf (&str, fmt, ap);
	va_end (ap);
	if (str == NULL) assert (!"Str is NULL");
	int system_ret = system (str);
	free (str);
	return system_ret;
}

int fexists (char *fname) {
	if (access (fname, F_OK) == -1) return 0;
	return 1;
}

static void open_file_cat_or_print_path (char *this_file_dir,
		enum versions version, enum modifiers modifiers, char *EDITOR) {

	char *filename = scratch;
	sprintf (filename, "%s/%s", this_file_dir, version_strs[version]);
	if (modifiers & MODIFIER_PRINT)
		printf ("%s\n", filename);
	else if (modifiers & MODIFIER_CAT) {
		if (cat_paths (stdout, (const char *[]) {filename}, 1, 0))
			err (1, "%s", filename);
	} else {
		if (!EDITOR) {
			errx (1, "\
Define editor in your shell first, eg \"EDITOR=/usr/bin/nano\"");
		}
		// Not checking the return value, can't be bothered to figure it
		// out.
		systemf ("%s %s", EDITOR, filename);
	}
}

// NOTE: I don't know why I did it like this.
void copy_file (char *from, char *to) {
	int copy_from = open (from, O_RDONLY, PERMISSIONS);
	if (copy_from == -1)
		err (1, "Couldn't open %s", from);
	int copy_to = open (to, O_WRONLY | O_CREAT | O_TRUNC, PERMISSIONS);
	if (copy_to == -1)
		err (1, "Couldn't open %s", to);

	for (;;) {
		ssize_t ret = sendfile (copy_to, copy_from, 0, BUFSIZ);
		if (ret == -1)
			err (1, "read of %s failed", from);
		else if (ret == 0)
			break;
	}
	close (copy_from);
	close (copy_to);
}

bool files_are_equal (char *a, char *b) {
	FILE *a_fp = fopen (a, "r");
	if (!a_fp) return 0;
	FILE *b_fp = fopen (b, "r");
	if (!b_fp) return 0;

	fseek (a_fp, 0, SEEK_END);
	fseek (b_fp, 0, SEEK_END);
	if (ftell (a_fp) != ftell (b_fp)) {
		return 0;
	}
	rewind (a_fp);
	rewind (b_fp);

	int c_a, c_b;
	while ((c_a = fgetc (a_fp)) != EOF) {
		c_b = fgetc (b_fp);
		if (c_a != c_b) return 0;
	}

	return 1;
}

static void print_state (char *file) {
	bool adulterated_is_equal = files_are_equal (file,
			version_strs[VERSION_ADULTERATED]);
	bool clean_is_equal = files_are_equal (file,
			version_strs[VERSION_CLEAN]);
	if (clean_is_equal && adulterated_is_equal)
		errx (1, "%s is equal to both clean and adulterated.", file);
	else if (!clean_is_equal && !adulterated_is_equal)
		errx (1, "%s is not equal to either clean or adulterated.", file);
	else if (adulterated_is_equal)
		printf ("Adulterated\n");
	else
		printf ("Clean\n");
}

static void mkdir_ignore_exist (char *dir, mode_t permissions) {
	mkdir (dir, permissions);
	if (errno && errno != EEXIST) {
		err (1, "mkdir failed");
	}
	errno = 0;
}

bool getyn (char *fmt, ...) {
	bool res = 0;
	va_list ap;
	va_start (ap, fmt);
	char *msg;
	vasprintf (&msg, fmt, ap);
	va_end (ap);
	int c;
	while (printf ("%s ", msg)) {
		c = fgetc (stdin);
		if (c == 'y') {
			res = 1;
			break;
		} else if (c == 'n') {
			res = 1;
			break;
		}
	}
	fputc ('\n', stdout);
	free (msg);
	return res;
}

__attribute__((noreturn))
static void init_or_die (char *file, char *this_file_dir) {
	/* bool yn = getyn ("Init, ie copy %s to %s/%s? ", */
	/* 		file, this_file_dir, version_strs[VERSION_CLEAN]); */
	/* if (!yn) exit (0); */
		fprintf (stderr, "\
Initting %s:\n\
    Make your changes to %s/adulterated,\n\
    then run \"adultery %s on\".\n",
file, this_file_dir, file);
	copy_file (file, version_strs[VERSION_CLEAN]);
	copy_file (file, version_strs[VERSION_ADULTERATED]);

	/* printf ("Make your changes in %s/%s or with %s %s --adulterated\n", */
	/* 		this_file_dir, version_strs[VERSION_ADULTERATED], */
	/* 		progname, file); */
	exit (0);
}

char *make_this_script_dir_str (char *path) {
	char *res = strdup (basename (path));
	return res;
}


// Just makes the dirname, eg, produces tmp-this-that from /tmp/this/that,
// not /usr/share/adultery/tmp-this-that
char *make_this_file_dir_str (char *path) {
	char *res = strdup (path);
	replace_char (res, strlen (res), '/', '-');

	return res;
}

int main (int argc, char **argv) {

	// Allocate this, cause PATH_MAX is big.
	// Data dir is eg /usr/local/share/adultery
	char *data_dir = xmalloc (sizeof *data_dir * PATH_MAX);
	get_data_dir (data_dir, 256, ROOT_DATA_DIR, DEFAULT_USER_DATA_DIR);
	mkdir_ignore_exist (data_dir, PERMISSIONS);

	// This doesn't deal with if the directory exists and isn't
	// a directory.

	struct args args = get_args (argc, argv);

	if (!(args.modifiers & MODIFIER_IS_SCRIPT)
			&& !fexists (args.file_or_script))
		err (1, "%s", args.file_or_script);

	if (chdir (data_dir) && errno)
		err (1, "Couldn't CD to %s", data_dir);
	// This is, eg, "etc-hosts" if we're dealing with files. It's, eg,
	// my-script-sh if it's scripts.
	char *this_file_dir =
		args.modifiers & MODIFIER_IS_SCRIPT?
			make_this_script_dir_str (args.file_or_script):
			make_this_file_dir_str (args.file_or_script);

	bool do_init = 0;

	// If the dir doesn't exist you haven't run the program on
	// this file yet.
	if (!fexists (this_file_dir)) {
		// Can't do init here because don't want to CD into this_file_dir
		// so no mallocing of strings and can't, because this_file_dir
		// doesn't exist yet.
		do_init = 1;
		if (mkdir (this_file_dir, PERMISSIONS) && errno)
			err (1, "Couldn't make %s", this_file_dir);
	} else
		if (!args.action)
			errx (1, "You haven't said to do anything");

	// Changing directory to keep from more mallocing.
	if (chdir (this_file_dir) && errno)
		err (1, "Couldn't CD to %s", this_file_dir);

	if (do_init || !fexists ("clean"))
		init_or_die (args.file_or_script, this_file_dir);

	char *EDITOR = getenv ("EDITOR");

	switch (args.action) {
		case ACTION_ON:
			if (!fexists (version_strs[VERSION_ADULTERATED])
					|| files_are_equal (version_strs[VERSION_CLEAN],
						version_strs[VERSION_ADULTERATED])) {
				errx (1, "\
It looks like you haven't made an adulterated file. Do %s --adulterated.",
						argv[0]);
			}
			if (args.modifiers & MODIFIER_IS_SCRIPT)
				system ("./adulterated");
			else
				copy_file (version_strs[VERSION_ADULTERATED],
						args.file_or_script);
			break;
		case ACTION_OFF:
			if (args.modifiers & MODIFIER_IS_SCRIPT)
				system ("./clean");
			else
				copy_file (version_strs[VERSION_CLEAN],
						args.file_or_script);
			break;

		case ACTION_ADULTERATED:
			open_file_cat_or_print_path (this_file_dir, VERSION_ADULTERATED,
					args.modifiers, EDITOR);
			break;
		case ACTION_CLEAN:
			open_file_cat_or_print_path (this_file_dir, VERSION_CLEAN,
					args.modifiers, EDITOR);
			break;
		case ACTION_LIST:
			print_files_in_dir (data_dir, S_IFDIR, replace_char);
			break;
		case ACTION_STATE:
			if (args.modifiers & MODIFIER_IS_SCRIPT)
				errx (1, "Can't do --state with scripts. Unless I asked you \
to write a \"check\" script");
			print_state (args.file_or_script);
			break;
		default:
			break;
	}
}
