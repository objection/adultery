## What is this?

This is a script that lets you toggle a file between two versions. On
Linux, in any case, but probably all the \*nixes.

When you run it for the first time on a file, it creates a directory
with two files in it, both copies of the original; one called "clean"
and the other called "adulterated". Subsequent invocations will copy
either "clean" or "adulterated" to the file, depending on your choice.

## Example

Say, like me, you want to keep yourself from wasting time on YouTube.
You might want to use /etc/hosts to block it. The way you do this, if
you didn't know, is to add this to the file:

```
127.0.0.1 www.youtube.com
::1 www.youtube.com
```

But you're weak. You know you'll want to watch YouTube on occasion.
You'd like to switch between a "clean" version of the file and your
"adulterated" one.

With Adultery, you can:

```
sudo adultery /etc/hosts
```

This will create the directory "etc-hosts" in
/usr/local/share/adultery. Then it will ask you if you want to edit
it.

Once you've made your changes, you can run the program again, like:

```
sudo adultery /etc/hosts on
```

Or:

```
sudo adultery /etc/hosts off
```

It has to be sudo in this example because you need sudo to write to
/etc/hosts.

"on" (or "1", or "true") copies the adulterated file to /etc/hosts;
"off" (or "0", or "false") will copy the clean one.

I bet you're thinking, "if he's struggling with some kind of YouTube
addiction, why would he write a script that makes it easier to get
back on YouTube?" I mean, making myself add the lines manually would
be a better deterrent, wouldn't it?

I've found using cron to run the script hourly is useful. Recently
I've been using a systemd timer. It's useful to be able to `systemd
status` your service, and see its messages with journalctl.

I've done:

```ini
[Unit]
Description=Run adultery to ban YouTube

[Service]
ExecStart=/usr/local/bin/adultery /etc/hosts on

[Install]
WantedBy=default.target
```

... in /etc/systemd/system/ban_internet.service.

And in /etc/systemd/system/ban_internet.timer:

```ini
[Unit]
Description=Run adultery regularly

[Timer]
OnCalendar=*-*-* *:00:00
Unit=ban_internet.service

[Install]
WantedBy=default.target
```

It seems to work. That string after OnCalendar means every hour of
every day.

This was written in Bash to begin with, but I don't know Bash, so I
rewrote it in C. The original's in ./abandoned.
