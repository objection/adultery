#!/bin/bash

# Exit if error
# set -o errexit
# Failed pipe returns the exit code of the last command executed
set -o pipefail
# Error if you try to use undeclared variables
set -o nounset

DEBUG=${DEBUG:-}
[[ $DEBUG ]] && set -o xtrace

PROGNAME=adultery
CLEAN_FILENAME=clean
ADULTERATED_FILENAME=adulterated

# The file the FILE'S CLEAN and ADULTERATED file go into
DATA_DIR=

# If you're root or using sudo, DATA_DIR /usr/local/share/adultery, else
# it's $XDG_DATA_HOME/adultery if $XDG_DATA_HOME is defined or
# ~/.local/share/adultery if not. The reason I'm doing this is so I can
# do "sudo adultery /etc/hosts off". If DATA_DIR was
# $HOME/.local/share/adultery you'd have to become root to edit the files
# in it (because root's home dir is /root and regular users can't get in).
# Putting it in /usr/local ...  means you can edit it them with sudo.
# I know I'm not doing this quite right.

ROOT_DATA_DIR=/usr/local/share/$PROGNAME
XDG_DATA_HOME=${XDG_DATA_HOME:-}
USER_DATA_DIR=${XDG_DATA_HOME:-$HOME/.local/share}
USER_DATA_DIR="${USER_DATA_DIR}/$PROGNAME"
if [[ $EUID == 0 ]]; then DATA_DIR=$ROOT_DATA_DIR
else DATA_DIR=$USER_DATA_DIR
fi
THIS_FILE_DIR=


die() {
	echo "$1"
	exit 1
}

function usage {
	exit_status="${1:-}" 
    echo "usage $0 [-hs] FILE [MODE]"
    echo "\
    Swap between CLEAN and ADULTERATED versions of a file.
    -a  --adulterated   Open FILE's adulterated version
    -c  --clean         Open FILE's clean version.
    -h  --help          Print this message.
    -l  --list          Show the files you've run this program on.
    -p  --path         Print FILE's path instead of opening it. With
                            --clean or --adulterated, print those instead.
    -s  --state         Print \"on\" if the $ADULTERATED_FILENAME is in use or
                            \"off\" if the $CLEAN_FILENAME file is in use.

    MODE is \"on\"/\"off\"/\"toggle\"

    When you run this script on a FILE, it creates a directory inside
    /usr/local/share/adultery if you're root or ~/.local/share/adultery if
    you're a regular user. This directory has the name of file with the
    initial '/' removed and the rest replaced with hyphens, eg,
    \"etc-hosts\". Inside this directory it puts two copies of the FILE,
    $CLEAN_FILENAME and $ADULTERATED_FILENAME. Then it asks you to edit
    it. After that, when you run adultery FILE <on|off> it replace FILE
    with CLEAN -- if \"on\" -- or ADULTERATED -- if \"off\".

    If you don't supply an option you must supply a mode."

	exit $exit_status
}

function get_y_n {
	local ret=
	read -rp "$1" -n 1 -a ret
	echo "\$1 = $1"
	echo "ret = ${ret[0]}"
	if [[ ${ret[0]} == 'y' || ${ret[0]} == 'Y' ]]; then
		echo HERE
		return 1
	elif [[ ${ret[0]} == 'n' || ${ret[0]} == 'N' ]]; then
		return 0;
	else
		die "Only 'y' or 'n' allowed"
	fi
}

# Checks to see if FILE is the same as the CLEAN file or the ADULTERATED
# file. If neither match, presumably the FILE has been edited directly:
# in which case, use diff to figure our whether CLEAN or ADULTERATED
# match most closely.
function get_state {
	if [[ ! $(diff "$ADULTERATED_FILENAME" "$file") ]]; then
		return 0
	elif [[ ! $(diff "$CLEAN_FILENAME" "$file") ]]; then
		return 1
	else
		local clean_lines_difference
		clean_lines_difference=$(diff --suppress-common-lines -b -B \
			$CLEAN_FILENAME "$file" | wc -l)
		local adulterated_lines_difference
		adulterated_lines_difference=$(diff --suppress-common-lines -b \
			-B $ADULTERATED_FILENAME "$file" | wc -l)
		if [[ $clean_lines_difference > $adulterated_lines_difference ]]; then
			return 2
		elif [[ $adulterated_lines_difference > $clean_lines_difference ]]; then
			return 3
		else
			return 4
		fi
	fi
}

# Print the results of get_state
function show-state {
	get_state
	local state=$?
	if [[ $state == 0 ]]; then
		echo "It's on"
	elif [[ $state == 1 ]]; then
		echo "It's off"
	else
		# how much is the file like adulterated?
		# how much is the file like clean?
		if [[ $state == 2 ]]; then
			echo "It's most like $ADULTERATED_FILENAME"
		elif [[ $state == 3 ]]; then
			echo "It's most like $CLEAN_FILENAME"
		else
			echo "$CLEAN_FILENAME and $ADULTERATED_FILENAME are equally unlike $file"
		fi
	fi
}

# If $mode is "on" or "1", copy ADULTERATED to FILE.
# Else if $mode is "off" or "0", copy CLEAN to FILE.
function do_switcheroo {
	if [[ ! $(diff "$CLEAN_FILENAME" "$ADULTERATED_FILENAME") ]]; then
		die "You haven't made changes to the $ADULTERATED_FILENAME file: it \
and the $CLEAN_FILENAME file are the same"
	fi
	local action=
	if [[ $mode == on || $mode == 1 ]]; then
		action=on
	elif [[ $mode == off || $mode == 0 ]]; then
		action=off
	elif [[ $mode == toggle || $mode == t ]]; then
		state=$(get_state)
		if [[ $state == on ]]; then
			action=off
		elif [[ $state == off ]]; then
			action=on
		else
			die "Can't toggle: files have been changed"
		fi
	else
		die "MODE needs to be \"on\"/\"1\" or \"off\"/\"0\""
	fi
	if [[ $action == on ]]; then
		if [[ ! $(diff $ADULTERATED_FILENAME "$file") ]]; then
			die "\
It's already on, ie $ADULTERATED_FILENAME and $file are the same"
		else
			cp "$ADULTERATED_FILENAME" "$file"
		fi
	elif [[ $action == off ]]; then
		if [[ ! $(diff $CLEAN_FILENAME "$file") ]]; then
			die "It's already off, ie $CLEAN_FILENAME and $file are the same"
		else
			cp "$CLEAN_FILENAME" "$file"
		fi
	fi
}

declare -A opts=([show-state]="" [list]="" [show_adulterated]="" \
	[show_clean]="" [show-path]="" )

PERMUTED_ARGS=$(getopt -o 'achlps' --long "adulterated,clean,help,list,print,state" -- "$@")
eval set -- "$PERMUTED_ARGS"

while true; do
    case "$1" in
		-a|--adulterated) opts[show_adulterated]=1; shift;;
		-c|--clean) opts[show_clean]=1; shift;;
        -h|--help) usage 0;;
		-l|--list)
			files=$(ls "$DATA_DIR")
			for i in $files; do
				echo "/$i" | sed 's:-:/:g'
			done
			exit 0;;
		-p|--path) opts[show-path]=1; shift;;
        -s|--state) opts[show-state]=1; shift;;
        --) shift; break;;
        *) echo "$1 is not an option"; exit 1;;
    esac
done

file="${1:-}"
mode="${2:-}"
if [[ ! $file ]]; then
	usage 1
fi
[[ "$file" ]] && file=$(realpath "$file")
if test -z "$mode" -a -z "${opts[show_adulterated]}" -a -z "${opts[show_clean]}" \
	-a -z "${opts[show-path]}" -a -z "${opts[show-state]}"; then
	usage 1
fi

# making DATA_DIR/THIS_FILE_DIR, which is the path
# of the file with the / changed to - and any leading -
# removed
mkdir "$DATA_DIR" > /dev/null 2>&1 || true
THIS_FILE_DIR=$(echo "$file" | sed 's:/:-:g' | sed 's/^-//g')
cd "$DATA_DIR" || die "cd failed"
if [[ ! -e "$THIS_FILE_DIR" ]]; then
	echo "Creating $DATA_DIR/$THIS_FILE_DIR"
fi
mkdir "$THIS_FILE_DIR" > /dev/null 2>&1  || true
cd "$THIS_FILE_DIR" || die "cd failed"

# If there's no file called "file" then you haven't run
# the script on this file yet, so copy it: this is the
# saved original""

if [[ ! -e "$CLEAN_FILENAME" ]]; then
	echo "Copying $file to $DATA_DIR/$THIS_FILE_DIR/$CLEAN_FILENAME"
	cp "$file" "$CLEAN_FILENAME"
fi

if [[ ! -e "$ADULTERATED_FILENAME" ]]; then
	echo "Copying $DATA_DIR/$THIS_FILE_DIR/$CLEAN_FILENAME to $DATA_DIR/\
$THIS_FILE_DIR/$ADULTERATED_FILENAME"
	cp "$CLEAN_FILENAME" "$ADULTERATED_FILENAME"
	echo here
	get_y_n "Edit now? "
	whatever=$?
	if [[ $whatever == 1  ]]; then
		echo here?
	# [[ $? == 1 ]] doesn't work here. Why?
		$EDITOR "$DATA_DIR/$THIS_FILE_DIR/$ADULTERATED_FILENAME"
	fi
	# Just exit here; can't do anything else if no ADULTERATED_FILENAME
	exit 0
fi

if [[ ${opts[show_adulterated]}  ]]; then
	if [[ ${opts[show-path]} ]]; then
		echo "$DATA_DIR/$THIS_FILE_DIR/$ADULTERATED_FILENAME"
	else
		echo "$DATA_DIR"
		$EDITOR "$DATA_DIR/$THIS_FILE_DIR/$ADULTERATED_FILENAME"
	fi
	exit 0
elif [[ ${opts[show_clean]} ]]; then
	if [[ ${opts[show-path]} ]]; then
		echo "$DATA_DIR/$THIS_FILE_DIR/$CLEAN_FILENAME"
	else
		$EDITOR "$DATA_DIR/$THIS_FILE_DIR/$CLEAN_FILENAME"
	fi
	exit 0
elif [[ ${opts[show-path]} ]]; then
	echo "$file"
	exit 0
elif [[ ${opts[show-state]} ]]; then
	show-state "$@"
	exit 0
elif [[ $mode ]]; then
	do_switcheroo
else
	usage
fi
